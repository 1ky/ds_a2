#ifndef Stack_h
#define Stack_h

#include "Node.h"

using namespace std;

class Stack {
private:
	Node* frontPtr;

public:
	Stack(void);
	void push(long, string, string, string, float);
	long pop(void);
	Node* get(long);
	void showNodes(void);
};

Stack::Stack(void) {
	frontPtr = new Node();
}

void Stack::push(long id, string n, string sn, string m, float g) {
	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(frontPtr->getNext());
	frontPtr->setNext(newPtr);
}

long Stack::pop(void) {
	long id = frontPtr->getIdNo();

	Node* tempPtr = frontPtr->getNext();
	frontPtr->setNext(tempPtr->getNext());
	delete tempPtr;

	return id;
}

Node* Stack::get(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) {
			return iPtr;
		}
		iPtr = iPtr->getNext();
	}
	return nullptr;
}

void Stack::showNodes(void) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		cout << iPtr->getIdNo() << endl;
		cout << iPtr->getName() << endl;
		cout << iPtr->getSurname() << endl;
		cout << iPtr->getMajor() << endl;
		cout << iPtr->getGpa() << endl;
		cout << endl;

		iPtr = iPtr->getNext();
	}
}

#endif
