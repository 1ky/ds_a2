#ifndef DEQUE_H
#define DEQUE_H

#include <iostream>
#include "Node.h"

using namespace std;

class Deque {
	private:
		Node* frontPtr;
	public:
		Deque(void);
		void enqueueHead(long, string, string, string, float);
		void enqueueTail(long, string, string, string, float);
		long dequeueHead(void);
		long dequeueTail(void);
		bool find(long);
		void showNodes(void);
		bool isEmpty(void);
		int number(void);
};

Deque::Deque(void) {
	frontPtr = new Node(void);
}

void Deque::enqueueHead(long l, string a, string b, string c, float f) {
	Node* newPtr = new Node(l, a, b, c, f);
	newPtr->setNext(frontPtr->getNext());
	frontPtr->setNext(newPtr);
}

void Deque::enqueueTail(long l, string a, string b, string c, float f) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* newPtr = new Node(l, a, b, c, f);
	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

long Deque::dequeueHead(void) {
	long id = frontPtr->getIdNo();

	Node* tempPtr = frontPtr->getNext();
	frontPtr->setNext(tempPtr->getNext());
	delete tempPtr;

	return id;
}

long Deque::dequeueTail(void) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext()->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* tempPtr = iPtr->getNext();
	iPtr->setNext(tempPtr->getNext());
	long id = tempPtr->getIdNo();
	delete tempPtr;

	return id;
}

bool Deque::find(long l) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == l)
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

void Deque::showNodes(void) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		cout << iPtr->getIdNo() << endl;
		cout << iPtr->getName() << endl;
		cout << iPtr->getSurname() << endl;
		cout << iPtr->getMajor() << endl;
		cout << iPtr->getGpa() << endl;
		cout << endl;

		iPtr = iPtr->getNext();
	}
}

bool Deque::isEmpty(void) {
	if (frontPtr == nullptr)
		return true;

	return false;
}

int Deque::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

#endif
