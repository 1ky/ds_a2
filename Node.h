#ifndef Node_h
#define Node_h

#include <string.h>

using namespace std;

class Node {
private:
	long idNo;
	string name;
	string surname;
	string major;
	float gpa;

	Node* Next;

public:
	Node();
	Node(long id, string n, string sn, string m, float g);

	void setIdNo(long id);
	long getIdNo();

	void setName(string n);
	string getName();

	void setSurname(string sn);
	string getSurname();

	void setMajor(string m);
	string getMajor();

	void setGpa(float g);
	float getGpa();

	void setNext(Node* ptr);
	Node* getNext();
};

Node::Node() {
	string n = "";

	setIdNo(0);
	setName(n);
	setSurname(n);
	setMajor(n);
	setGpa(0);
	setNext(nullptr);
}

Node::Node(long id, string n, string sn, string m, float g) {
	setIdNo(id);
	setName(n);
	setSurname(sn);
	setMajor(m);
	setGpa(g);
	setNext(nullptr);
}

void Node::setIdNo(long id) {
	idNo = id;
}

long Node::getIdNo() {
	return idNo;
}

void Node::setName(string n) {
	name = n;
}

string Node::getName() {
	return name;
}

void Node::setSurname(string sn) {
	surname = sn;
}

string Node::getSurname() {
	return surname;
}

void Node::setMajor(string m) {
	major = m;
}

string Node::getMajor() {
	return major;
}

void Node::setGpa(float g) {
	gpa = g;
}

float Node::getGpa() {
	return gpa;
}

void Node::setNext(Node* ptr) {
	Next = ptr;
}

Node* Node::getNext() {
	return Next;
}

#endif
