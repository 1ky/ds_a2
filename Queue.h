#ifndef Queue_h
#define Queue_h

#include "Node.h"

using namespace std;

class Queue {
private:
	Node* frontPtr;
public:
	Queue(void);
	void enqueue(long, string, string, string, float);
	long dequeue(void);
	bool find(long);
	void showNodes(void);
	bool isEmpty(void);
	int number(void);
};

Queue::Queue(void) {
	frontPtr = new Node();
}

void Queue::enqueue(long id, string n, string sn, string m, float g) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

long Queue::dequeue(void) {
	int id = frontPtr->getIdNo();

	Node* tempPtr = frontPtr->getNext();
	frontPtr->setNext(tempPtr->getNext());
	delete tempPtr;

	return id;
}

bool Queue::find(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) 
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

void Queue::showNodes(void) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		cout << iPtr->getIdNo() << endl;
		cout << iPtr->getName() << endl;
		cout << iPtr->getSurname() << endl;
		cout << iPtr->getMajor() << endl;
		cout << iPtr->getGpa() << endl;
		cout << endl;

		iPtr = iPtr->getNext();
	}
}

bool Queue::isEmpty(void) {
	if (frontPtr == nullptr)
		return true;

	return false;
}

int Queue::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

#endif
