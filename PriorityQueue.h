#ifndef PriorityQueue_h
#define PriorityQueue_h

#include "Node.h"

using namespace std;

class PriorityQueue {
private:
	Node* frontPtr;
public:
	PriorityQueue(void);
	void enqueue(long, string, string, string, float);
	long dequeue(void);
	bool find(long);
	void showNodes(void);
	bool isEmpty(void);
	int number(void);
};

PriorityQueue::PriorityQueue(void) {
	frontPtr = new Node();
}

void PriorityQueue::enqueue(long id, string n, string sn, string m, float g) {
	Node* iPtr = frontPtr;
	Node* newPtr = new Node(id, n, sn, m, g);
	
	while (iPtr->getNext() != nullptr && newPtr->getIdNo() >= iPtr->getNext()->getIdNo()) {
		iPtr = iPtr->getNext();
	}

	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

long PriorityQueue::dequeue(void) {
	int id = frontPtr->getIdNo();

	Node* tempPtr = frontPtr->getNext();
	frontPtr->setNext(tempPtr->getNext());
	delete tempPtr;

	return id;
}

bool PriorityQueue::find(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) 
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

void PriorityQueue::showNodes(void) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		cout << iPtr->getIdNo() << endl;
		cout << iPtr->getName() << endl;
		cout << iPtr->getSurname() << endl;
		cout << iPtr->getMajor() << endl;
		cout << iPtr->getGpa() << endl;
		cout << endl;

		iPtr = iPtr->getNext();
	}
}

bool PriorityQueue::isEmpty(void) {
	if (frontPtr == nullptr)
		return true;

	return false;
}

int PriorityQueue::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

#endif
