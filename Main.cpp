#include <iostream>
#include "Node.h"
#include "LinkedList.h"

using namespace std;

int main() {
	LinkedList ll;
	string name = "John";
	ll.addHead(1, name, name, name, 3.5);
	ll.getNode(1);
	ll.addTail(2, name, name, name, 3.5);
	ll.showNodes();
}
