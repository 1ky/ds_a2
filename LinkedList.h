#ifndef LinkedList_h
#define LinkedList_h

#include <iostream>
#include "Node.h"

using namespace std;

class LinkedList {
private:
	Node* frontPtr;
public:
	LinkedList(void);
	void addTail(long, string, string, string, float);
	void addHead(long, string, string, string, float);
	void delNode(long);
	Node* getNode(long);
	void showNodes(void);
};

LinkedList::LinkedList(void) {
	frontPtr = new Node();
}

void LinkedList::addTail(long id, string n, string sn, string m, float g) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

void LinkedList::addHead(long id, string n, string sn, string m, float g) {
	//std::cout << "hh";
	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(frontPtr->getNext());
	frontPtr->setNext(newPtr);
}

void LinkedList::delNode(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}
		iPtr = iPtr->getNext();
	}
}

Node* LinkedList::getNode(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) {
			return iPtr;
		}
		iPtr = iPtr->getNext();
	}
	return nullptr;
}

void LinkedList::showNodes(void) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		cout << iPtr->getIdNo() << endl;
		cout << iPtr->getName() << endl;
		cout << iPtr->getSurname() << endl;
		cout << iPtr->getMajor() << endl;
		cout << iPtr->getGpa() << endl;
		cout << endl;

		iPtr = iPtr->getNext();
	}
}

#endif
